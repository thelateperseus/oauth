package nathan.authserver.config;

import java.util.Arrays;
import java.util.UUID;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;

import nathan.authserver.user.CustomUserDetailsManager;
import nathan.authserver.user.UserAccount;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http
            .authorizeRequests()
                .antMatchers("/", "/login", "/error").permitAll()
                .anyRequest().authenticated()
                .and()
            .formLogin()
                .loginPage("/login")
                .defaultSuccessUrl("/home")
                .failureUrl("/login?error=true")
                .and()
            .logout()
                .logoutSuccessUrl("/");
    }

    @Bean
    public UserDetailsService customUserDetailsService() {
        final UserAccount user = new UserAccount();
        user.setId(UUID.randomUUID().toString());
        user.setUsername("user");
        user.setPassword("password1");
        user.setAuthorities(Arrays.asList(new SimpleGrantedAuthority("ROLE_USER")));
        user.setEmail("user@example.com");
        user.setFullName("Mr User");

        final UserAccount admin = new UserAccount();
        admin.setId(UUID.randomUUID().toString());
        admin.setUsername("admin");
        admin.setPassword("password1");
        admin.setAuthorities(Arrays.asList(new SimpleGrantedAuthority("ROLE_USER")));
        admin.setEmail("admin@example.com");
        admin.setFullName("Ms Admin");

        return new CustomUserDetailsManager(user, admin);
    }
}