package nathan.authserver.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationManager;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationProcessingFilter;
import org.springframework.security.oauth2.provider.error.OAuth2AccessDeniedHandler;
import org.springframework.security.oauth2.provider.error.OAuth2AuthenticationEntryPoint;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;

@Configuration
@EnableWebSecurity
@Order(1)
public class UserInfoSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired private ClientDetailsService clientDetails;
    @Autowired private TokenStore tokenStore;

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        final AuthenticationEntryPoint authenticationEntryPoint = new OAuth2AuthenticationEntryPoint();

        final OAuth2AuthenticationProcessingFilter resourcesServerFilter = new OAuth2AuthenticationProcessingFilter();
        resourcesServerFilter.setAuthenticationEntryPoint(authenticationEntryPoint );
        resourcesServerFilter.setAuthenticationManager(oauthAuthenticationManager(http));
        resourcesServerFilter.setStateless(true);

        http
            .antMatcher("/userinfo")
                .authorizeRequests()
                .anyRequest().authenticated()
                .and()
            .addFilterBefore(resourcesServerFilter, AbstractPreAuthenticatedProcessingFilter.class)
            .exceptionHandling()
                .accessDeniedHandler(new OAuth2AccessDeniedHandler())
                .authenticationEntryPoint(authenticationEntryPoint);
    }

    private AuthenticationManager oauthAuthenticationManager(final HttpSecurity http) {
        final OAuth2AuthenticationManager oauthAuthenticationManager = new OAuth2AuthenticationManager();

        final DefaultTokenServices tokenServices = new DefaultTokenServices();
        tokenServices.setTokenStore(tokenStore);
        tokenServices.setSupportRefreshToken(true);
        tokenServices.setClientDetailsService(clientDetails);

        oauthAuthenticationManager.setResourceId("oauth2-resource");
        oauthAuthenticationManager.setTokenServices(tokenServices);
        oauthAuthenticationManager.setClientDetailsService(clientDetails);
        return oauthAuthenticationManager;
    }
}
