package nathan.authserver.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import nathan.authserver.user.IdTokenEnhancer;
import nathan.authserver.user.UserAccount;

@RestController
public class UserInfoController {
    @Autowired private IdTokenEnhancer idTokenEnhancer;

    @GetMapping("/userinfo")
    public Map<String, Object> user(final OAuth2Authentication principal) {
        final UserAccount userAccount = (UserAccount)principal.getUserAuthentication().getPrincipal();
        return idTokenEnhancer.buildClaims(userAccount);
    }
}
