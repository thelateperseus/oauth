package nathan.authserver.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomeController {
    @GetMapping(value = "/home")
    public ModelAndView home(final Authentication user) {
        final Map<String, Object> model = new HashMap<>();
        model.put("user", user.getPrincipal());
        return new ModelAndView("home", model);
    }
}
