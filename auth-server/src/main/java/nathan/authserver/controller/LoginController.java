package nathan.authserver.controller;

import javax.servlet.http.HttpSession;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.WebAttributes;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class LoginController {
    @GetMapping(value = "/login")
    public ModelAndView login(final HttpSession session) {
        final AuthenticationException authException =
            (AuthenticationException) session.getAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
        final ModelAndView mav = new ModelAndView("login");
        if (authException != null) {
            mav.addObject("errorMessage", authException.getMessage());
        }
        return mav;
    }
}
