package nathan.authserver.user;

import java.util.HashMap;
import java.util.Map;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.util.Assert;
import org.springframework.util.SerializationUtils;

public class CustomUserDetailsManager implements UserDetailsManager {

    private final Map<String, UserAccount> users = new HashMap<>();

    public CustomUserDetailsManager(final UserAccount... users) {
        for (final UserAccount user : users) {
            createUser(user);
        }
    }

    @Override
    public void createUser(final UserDetails user) {
        Assert.isTrue(!userExists(user.getUsername()), "user should not exist");

        users.put(user.getUsername().toLowerCase(), (UserAccount)user);
    }

    @Override
    public void deleteUser(final String username) {
        users.remove(username.toLowerCase());
    }

    @Override
    public void updateUser(final UserDetails user) {
        Assert.isTrue(userExists(user.getUsername()), "user should exist");

        users.put(user.getUsername().toLowerCase(), (UserAccount)user);
    }

    @Override
    public boolean userExists(final String username) {
        return users.containsKey(username.toLowerCase());
    }

    @Override
    public UserDetails loadUserByUsername(final String username)
            throws UsernameNotFoundException {
        final UserAccount user = users.get(username.toLowerCase());

        if (user == null) {
            throw new UsernameNotFoundException(username);
        }

        return (UserDetails) SerializationUtils.deserialize(SerializationUtils.serialize(user));
    }

    @Override
    public void changePassword(final String oldPassword, final String newPassword) {
        throw new UnsupportedOperationException();
    }

}
