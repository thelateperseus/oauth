package nathan.authserver.user;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Base64;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.security.jwt.crypto.sign.MacSigner;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class IdTokenEnhancer implements TokenEnhancer {
    public static final int ID_TOKEN_VALIDITY_SECONDS = 60;

    private static final MacSigner hmac = new MacSigner("S980fgsigsfgSFG8SgfSr7g0sGSFgsgh31".getBytes());

    @Override
    public OAuth2AccessToken enhance(final OAuth2AccessToken accessToken,
                                     final OAuth2Authentication authentication) {
        if (!accessToken.getScope().contains("openid")) {
            return accessToken;
        }
        final UserAccount userAccount =
            (UserAccount)authentication.getUserAuthentication().getPrincipal();
        final Map<String, Object> claims = buildClaims(userAccount);

        try {
            final MessageDigest hashFunction = MessageDigest.getInstance("SHA-256");
            final byte[] hashValue =
                hashFunction.digest(accessToken.getValue().getBytes(StandardCharsets.US_ASCII));
            final byte[] leftHalfOfHashValue = new byte[hashValue.length/2];
            System.arraycopy(hashValue, 0, leftHalfOfHashValue, 0, leftHalfOfHashValue.length);
            final String accessTokenHash =
                Base64.getUrlEncoder().withoutPadding().encodeToString(leftHalfOfHashValue);
            claims.put("at_hash", accessTokenHash);
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }

        // TODO auth_time

        final long currentTimeMillis = System.currentTimeMillis();
        final long currentTimeSeconds = currentTimeMillis/1000;

        claims.put("iss", "http://localhost:8080/");
        claims.put("aud", authentication.getOAuth2Request().getClientId());
        claims.put("iat", currentTimeSeconds);
        claims.put("exp", currentTimeSeconds + ID_TOKEN_VALIDITY_SECONDS);

        final String nonce = authentication.getOAuth2Request().getRequestParameters().get("nonce");
        if (!StringUtils.isEmpty(nonce)) {
            claims.put("nonce", nonce);
        }

        final Jwt jwt;
        try {
            jwt = JwtHelper.encode(new ObjectMapper().writeValueAsString(claims), hmac);
        } catch (final JsonProcessingException e) {
            throw new RuntimeException(e);
        }

        final Map<String, Object> additionalInfo = new HashMap<>();
        additionalInfo.put("id_token", jwt.getEncoded());
        ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(additionalInfo);

        return accessToken;
    }

    public Map<String, Object> buildClaims(final UserAccount userAccount) {
        final Map<String, Object> claims = new LinkedHashMap<>();
        claims.put("sub", userAccount.getId());
        claims.put("name", userAccount.getFullName());
        claims.put("preferred_username", userAccount.getUsername());
        claims.put("email", userAccount.getEmail());
        return claims;
    }
}
