<#assign navArea = "">
<#include "includes/base.ftl">
<#macro page_head>
  <title>Home</title>
</#macro>

<#macro main_content>
  <h1>Home</h1>

  You are home, ${user.fullName}.
</#macro>

<@display_page/>