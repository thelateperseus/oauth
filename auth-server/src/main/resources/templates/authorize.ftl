<#assign navArea = "">
<#include "includes/base.ftl">
<#macro page_head>
  <title>Home</title>
</#macro>

<#macro main_content>
  <h1>Authorize</h1>

  <form id="confirmationForm" name="confirmationForm" action="authorize" method="post">
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    <input name="user_oauth_approval" value="true" type="hidden" />
    <p>Do you authorize "${authorizationRequest.clientId}" at "${authorizationRequest.redirectUri}" to access your protected resources:</p>
    <ul>
      <#list authorizationRequest.scope as scope>
        <li>${scope}<input type="hidden" name="scope.${scope}" value="true"></li>
      </#list>
    </ul>
    <button class="btn btn-primary" type="submit">Approve</button>
  </form>
</#macro>

<@display_page/>