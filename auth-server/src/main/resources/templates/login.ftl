<#assign navArea = "">
<#include "includes/base.ftl">
<#macro page_head>
  <title>Login</title>
  <style>

  </style>
</#macro>

<#macro main_content>
<div class="container mt-4" style="max-width: 540px; ">
  <div class="card">
    <div class="card-header">Login with your passport account</div>
  
    <div class="card-body">
      <form action="/login" method="POST">
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    
        <div class="form-group">
          <label for="username">* Username</label>
          <input type="text"
            id="username" 
            name="username"
            class="form-control" 
            placeholder="Username"
            autofocus
            required >
        </div>
    
        <div class="form-group">
          <label for="password">* Password</label>
          <input type="password"
            id="password" 
            name="password"
            class="form-control" 
            placeholder="Password"
            required >
        </div>
    
        <#if RequestParameters.error?exists>
          <div class="alert alert-danger" role="alert">
            <#if errorMessage??>${errorMessage}<#else>Invalid Username or Password</#if>
          </div>
        </#if>
    
        <button class="btn btn-primary" type="submit" id="submitButton">Login</button>
      </form>
    </div>
  </div>
</div>
</#macro>

<@display_page/>