<#assign navArea = "">
<#include "includes/base.ftl">
<#macro page_head>
  <title>Feed</title>
</#macro>

<#macro main_content>
  <h1>Feed</h1>
  <div class="row">
    <div class="col-sm-2">User:</div>
    <div class="col-sm">${feed.user}</div>
  </div>
  <h2>Items</h2>
  <ul>
    <#list feed.items as item>
      <li>${item.title}</li>
    </#list>
  </ul>
</#macro>

<@display_page/>