<#assign navArea = "">
<#include "includes/base.ftl">
<#macro page_head>
  <title>Home</title>
</#macro>

<#macro main_content>
  <h1>Home</h1>

  <a href="feed" class="btn btn-primary">View Feed</a>
</#macro>

<@display_page/>