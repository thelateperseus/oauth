package nathan.client;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ClientController {
    @Autowired private OAuth2RestTemplate restTemplate;

    @GetMapping(value = "/feed")
    public ModelAndView feed() {
        final Map<String, Object> result =
            restTemplate.getForObject("http://localhost:8083/resource/rest/v1/feed", Map.class);
        final ModelAndView mav = new ModelAndView("feed");
        mav.addObject("feed", result);
        return mav;
    }
}
