package nathan.resourceserver.sso;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.oauth2.resource.FixedAuthoritiesExtractor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.jwt.Jwt;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.security.jwt.crypto.sign.MacSigner;
import org.springframework.security.oauth2.client.OAuth2RestOperations;
import org.springframework.security.oauth2.client.http.AccessTokenRequiredException;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;

import com.fasterxml.jackson.databind.ObjectMapper;

public class OidcAuthenticationProcessingFilter extends AbstractAuthenticationProcessingFilter {
    static final MacSigner hmac = new MacSigner("S980fgsigsfgSFG8SgfSr7g0sGSFgsgh31".getBytes());

    @Autowired private OAuth2RestOperations restTemplate;
    @Autowired private OidcClaimsPrincipalExtractor principalExtractor;

    public OidcAuthenticationProcessingFilter() {
        super("/login");
        setAuthenticationManager(new NoopAuthenticationManager());
    }

    @Override
    public void doFilter(final ServletRequest req, final ServletResponse res, final FilterChain chain)
            throws IOException, ServletException {
        super.doFilter(req, res, chain);
    }

    @Override
    public Authentication attemptAuthentication(final HttpServletRequest request,
                                                final HttpServletResponse response)
            throws AuthenticationException, IOException, ServletException {

        OAuth2AccessToken accessToken;
        try {
            accessToken = restTemplate.getAccessToken();
        } catch (final OAuth2Exception e) {
            throw new BadCredentialsException("Could not obtain access token", e);
        }
        try {
            final String idToken = accessToken.getAdditionalInformation().get("id_token").toString();
            if (idToken == null) {
                return null;
            }
            final Jwt tokenDecoded = JwtHelper.decodeAndVerify(idToken, hmac);
            final Map<String, Object> claims =
                new ObjectMapper().readValue(tokenDecoded.getClaims(), Map.class);
            verifyClaims(claims);
            final OidcClaimsPrincipal principal =
                (OidcClaimsPrincipal) principalExtractor.extractPrincipal(claims);

            // TODO create an AuthoritiesExtractor to lookup authorities
            final List<GrantedAuthority> authorities = new FixedAuthoritiesExtractor().extractAuthorities(claims);

            // Copied from UserInfoTokenServices.extractAuthentication()
            final String clientId = (String)claims.get("aud");
            final OAuth2Request oauth2Request = new OAuth2Request(null, clientId, null, true, null,
                    null, null, null, null);
            final UsernamePasswordAuthenticationToken usernameToken = new UsernamePasswordAuthenticationToken(
                    principal, "N/A", authorities);
            usernameToken.setDetails(claims);
            return new OAuth2Authentication(oauth2Request, usernameToken);
        } catch (final Exception e) {
            throw new BadCredentialsException("Could not obtain user details from token", e);
        }

    }

    public void verifyClaims(final Map claims) {
        final int exp = (int) claims.get("exp");
        final Date expireDate = new Date(exp * 1000L);
        final Date now = new Date();
        if (expireDate.before(now) ||
                !claims.get("iss").equals("http://localhost:8080/") ||
                !claims.get("aud").equals("demo-resource")) {
            throw new RuntimeException("Invalid claims");
        }
    }

    /** Copied from OAuth2ClientAuthenticationProcessingFilter */
    @Override
    protected void successfulAuthentication(final HttpServletRequest request, final HttpServletResponse response,
            final FilterChain chain, final Authentication authResult) throws IOException, ServletException {
        super.successfulAuthentication(request, response, chain, authResult);
        // Nearly a no-op, but if there is a ClientTokenServices then the token will now be stored
        restTemplate.getAccessToken();
    }

    /** Copied from OAuth2ClientAuthenticationProcessingFilter */
    @Override
    protected void unsuccessfulAuthentication(final HttpServletRequest request, final HttpServletResponse response,
            final AuthenticationException failed) throws IOException, ServletException {
        if (failed instanceof AccessTokenRequiredException) {
            // Need to force a redirect via the OAuth client filter, so rethrow here
            throw failed;
        }
        else {
            // If the exception is not a Spring Security exception this will result in a default error page
            super.unsuccessfulAuthentication(request, response, failed);
        }
    }

    private static class NoopAuthenticationManager implements AuthenticationManager {
        @Override
        public Authentication authenticate(final Authentication authentication) throws AuthenticationException {
            throw new UnsupportedOperationException("No authentication should be done with this AuthenticationManager");
        }
    }
}