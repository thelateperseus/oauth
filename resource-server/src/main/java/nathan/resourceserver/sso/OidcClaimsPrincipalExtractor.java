package nathan.resourceserver.sso;

import java.util.Map;

import org.springframework.boot.autoconfigure.security.oauth2.resource.PrincipalExtractor;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public class OidcClaimsPrincipalExtractor implements PrincipalExtractor {

    @Override
    public Object extractPrincipal(final Map<String, Object> map) {
        return new ObjectMapper().convertValue(map, OidcClaimsPrincipal.class);
    }

}
