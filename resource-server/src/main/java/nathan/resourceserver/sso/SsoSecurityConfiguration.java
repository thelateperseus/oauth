package nathan.resourceserver.sso;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.boot.autoconfigure.security.oauth2.resource.UserInfoRestTemplateFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;

@Configuration
@EnableOAuth2Sso
//@Order(101)
public class SsoSecurityConfiguration extends WebSecurityConfigurerAdapter {
    @Autowired private UserInfoRestTemplateFactory userInfoRestTemplateFactory;

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http
          .authorizeRequests()
              .antMatchers("/", "/login", "/error").permitAll()
              .anyRequest().authenticated()
              .and()
          .logout()
              .logoutSuccessUrl("/")
              .and()
          .addFilterBefore(oidcAuthenticationProcessingFilter(),
              AbstractPreAuthenticatedProcessingFilter.class);
    }

    @Bean
    public OidcAuthenticationProcessingFilter oidcAuthenticationProcessingFilter()
    {
        return new OidcAuthenticationProcessingFilter();
    }

    @Bean OAuth2RestTemplate oauth2RestTemplate()
    {
        return userInfoRestTemplateFactory.getUserInfoRestTemplate();
    }
}