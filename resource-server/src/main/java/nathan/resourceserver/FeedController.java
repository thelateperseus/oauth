package nathan.resourceserver;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class FeedController {
    @GetMapping(value = "/")
    public ModelAndView root() {
        return new ModelAndView("index");
    }

    @GetMapping(value = "/rest/v1/feed")
    public Map<String, Object> feedApi(final OAuth2Authentication user) {
        return getFeed(user);
    }

    @GetMapping(value = "/feed")
    public ModelAndView feedPage(final OAuth2Authentication user) {
        return new ModelAndView("feed", getFeed(user));
    }

    private Map<String, Object> getFeed(final OAuth2Authentication user) {
        final Map<String, Object> result = new HashMap<>();
        result.put("user", user.getUserAuthentication().getPrincipal());

        final List<Map<String,Object>> items = new ArrayList<>();
        result.put("items", items);

        final Map<String, Object> item1 = new HashMap<>();
        items.add(item1);
        item1.put("id", "42524524524");
        item1.put("title", "Something exciting happened today");
        item1.put("link", "http://www.example.com/42524524524/something-exciting-happened-today");

        final Map<String, Object> item2 = new HashMap<>();
        items.add(item2);
        item2.put("id", "89646984768647");
        item2.put("title", "Once upon a time there was a whole bunch of fish");
        item2.put("link", "http://www.example.com/89646984768647/once-upon-a-time-there-was-a-whole-bunch-of-fish");

        return result;
    }
}
